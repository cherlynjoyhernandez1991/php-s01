<?php
// [SECTION] Comment
/*
PHP comments are written exactly the same as JS comments
*/
/*
When writing any PHP code, always remeber to start with the opening php tag:

<?php

Closing with ?> is optional when ONLY writing PHP code
*/ 

// [SECTION] Echo
// Use to output data to the screen
// Always remember to end every PHP statememnt with a semicolon (;)

// echo "Hello World";

// [SECTION] Variables

$name = "John Smith";
$email = "johnsmith@gmail.com";

//[SECTION] Constants

define('PI', 3.1416);

// [SECTION] Data Types
$state = "New York";
$country = "USA";
$address =  $state . ', ' . $country;
$address2 = "$state, $country";

// Integers
$age = 31;
$headcount = 26;

// Floats
$grade = 98.2;
$distance = 1342.12;

// Boolean
$hasTravelledAbroad = true;

//Null
$spouse = null;

// Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);

//Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object)[
	'fullname' => "John Smith",
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => 'USA'
	]
];

// [Section] Operators

//Assignment Operator (=)
$x = 234;
$y = 123;

$islegalAge = true;
$isRegistered = false;


// [SECTION] Functions
function getFullName($firstName, $middleInitial, $lastName){
	return "$lastName, $firstName $middleInitial";
}


// [SECTION] Selection Control Structures - Conditional Statements

// If elseif else
function determineTyphoonIntensity($windSpeed) {
	if ($windSpeed < 30) {
		return "Not a typhoon yet";
	} elseif ($windSpeed <= 61) {
		return "Tropical depression detected";
	} elseif ($windSpeed >= 62 && $windSpeed <= 88) {
		return "Tropical storm detected";
	} elseif ($windSpeed >= 89 && $windSpeed <= 117) {
		return "Severe tropical storm detected";
	} else {
		return "Typhoon detected";
	}
}


// Ternary Operator
function isUnderAge($age) {
	return ($age < 18) ? true : false;
}


// Switch Statement
function determineUser($computerNumber) {
	switch ($computerNumber) {
		case '1':
			return 'Linus Torvalds';
			break;

		case '2':
			return 'Steave Jobs';
			break;
			
		case '3':
			return 'Sid Meier';
			break;

		default:
			return "The computer number $computerNumber is out of bounds";
			break;
	}
}