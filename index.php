<!-- PHP code can ve included in another file by using the require_once keyword. -->
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S01</title>
</head>
<body>
	<h1>Hello World</h1>

	<h2>Variables: </h2>
	<!-- Variables can be used to output data in double quotes while single quotes will not work -->
	<p><?php echo "Good day $name! Your email is $email."; ?></p>
	<!-- When echoing constants, omit the $ sign -->
	<p><?php echo PI; ?></p>
	<p><?php echo $address; ?></p>
	<p><?php echo $address2; ?></p>
	


	<h2>Dataypes: </h2>
	<p><?php echo $hasTravelledAbroad; ?></p>
	<p><?php echo $spouse; ?></p>
	<!-- Normal echoing of boollean and null varialbes will not make them visible to the use. Too see their types instead, we can use trhe following. -->
	<p><?php echo gettype($hasTravelledAbroad); ?></p>
	<p><?php echo gettype($spouse); ?></p>

	<!-- To see more detailed info on the variable -->
	<p><?php echo var_dump($hasTravelledAbroad); ?></p>
	<p><?php echo var_dump($spouse); ?></p>
	<p><?php print_r($grades); ?></p>

	<!-- Use the -> to access obj properties and => to assign obj values -->
	<p><?php echo $gradesObj->firstGrading; ?></p>
	<p><?php echo $personObj->address->state; ?></p>
	<p><?php echo $grades[3]; ?></p>

	<h2>Operators: </h2>
	<h3>Arithmetic Operators</h3>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Quotient: <?php echo $x / $y; ?></p>

	<h3>Equality Operators</h3>
	<p>Loose Equality Operators <?php var_dump($x == 234); ?></p>
	<p>Loose Inequality Operators <?php var_dump($x != 123);?></p>
	<p>Strict Equality Operators <?php var_dump($x === 123); ?></p>
	<p>Strict Inequality Operators <?php var_dump($x !== 234);?></p>

	<h3>Greater/Lesser Operators</h3>
	<p>Is lesser: <?php var_dump($x < $y); ?></p>
	<p>Is lesser (or equal): <?php var_dump($x <= $y); ?></p>

	<h3>Logical Operators</h3>
	<p>Are all requirements met?: <?php var_dump($islegalAge && $isRegistered)?></p>
	<p>Are Some requirements met?: <?php var_dump($islegalAge || $isRegistered)?></p>
	<p>Are Some requirements 'not' met?: <?php var_dump(!$islegalAge && !$isRegistered)?></p>

	<h2>Functions</h2>
	<p>Full Name: <?php echo getFullName("John", "J", "Smith"); ?></p>

	<h2>Selection Control Structures</h2>
	<h3>If Elseif Else</h3>
	<p><?php echo determineTyphoonIntensity(12); ?></p>

	<h3>Ternary Operators</h3>
	<p><?php var_dump(isUnderAge(78)); ?></p>
	<p><?php var_dump(isUnderAge(13)); ?></p>

	<h3>Switch Statement</h3>
	<p><?php echo determineUser(1); ?></p>
	<p><?php echo determineUser(2) ; ?></p>
</body>
</html>

<!-- https://gitlab.com/tuitt/students/batch187/resources -->